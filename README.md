# Shopping

Gerencia Lojas Shopping

## Serviços

### Inserir Segmento

####URL 

	POST /segmentos
	
####Corpo
`{
	"descricao" : "segmento1",
}`
				
Cadastrar segmento com a descrição informada.

### Inserir Loja

####URL

	POST /rest/lojas

####Corpo
	
	{
		"cnpj": "11917629000117",
		"piso": "1",
		"numero": "101",
		"segmentos": [1]
	}
				
Cadastrar lojas com a com os caompos informados. Somente o campo segmentos nao é obrigatrio.

### Excluir Loja

####URL

	DELETE /rest/lojas/{id}

O parâmetro `{id}` indica o código da loja a ser excluída.

### Auditar Loja

####URL

	GET /rest/lojas/audit/{id}

O parâmetro `{id}` indica o código da loja a ser auditada, mostrando quando a mesma foi excluída.


