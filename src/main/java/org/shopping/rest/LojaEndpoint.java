package org.shopping.rest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.shopping.model.Loja;
import org.shopping.model.Segmento;
import org.shopping.model.dto.LojaDto;
import org.shopping.rest.response.ResponseData;

/**
 * 
 */
@Stateless
@Path("/lojas")
public class LojaEndpoint {
	@PersistenceContext(unitName = "shopping-persistence-unit")
	private EntityManager em;

	@POST
	@Consumes("application/json")
	@Produces("application/json")	
	public Response create(LojaDto lojaDto) {
		
		ResponseData<LojaDto> responseData = new ResponseData<LojaDto>();
		responseData.setErrors(this.validate(lojaDto));		
		
		if(responseData.getErrors().size() > 0) {
			responseData.setData(lojaDto);
			return Response.status(Status.BAD_REQUEST).entity(responseData).build();
		}
		else {
			Loja lojaEnt = new Loja();
			this.convertFromDto(lojaDto, lojaEnt);
			lojaEnt.getSegmentos().forEach(seg -> seg.getLojas().add(lojaEnt));
			em.persist(lojaEnt);
			responseData.setData(this.convertToDto(lojaEnt));
			return Response.status(Status.CREATED).entity(responseData).build();
		}				
	}

	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteById(@PathParam("id") Long id) {
		
		Loja lojaEnt = em.find(Loja.class, id);
		if (lojaEnt == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		lojaEnt.getSegmentos().forEach(seg -> seg.getLojas().remove(lojaEnt));
		lojaEnt.setUpdated(new Date());
		em.persist(lojaEnt);
		em.flush();
		em.remove(lojaEnt);
		return Response.noContent().build();
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces("application/json")
	public Response findById(@PathParam("id") Long id) {
		
		TypedQuery<Loja> findByIdQuery = em.createQuery(
				"SELECT DISTINCT l FROM Loja l LEFT JOIN FETCH l.segmentos WHERE l.id = :entityId ORDER BY l.id", Loja.class)
				.setParameter("entityId", id);
		Loja entity;
		try {
			entity = findByIdQuery.getSingleResult();
		} catch (NoResultException nre) {
			entity = null;
		}
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		
		ResponseData<LojaDto> responseData = new ResponseData<LojaDto>();
		responseData.setData(this.convertToDto(entity));
		
		return Response.ok(responseData).build();
	}

	private Loja findBCnpj(String cnpj) {	
		
		TypedQuery<Loja> findByIdQuery = em.createQuery("SELECT l FROM Loja l WHERE l.cnpj = :cnpj", Loja.class).setParameter("cnpj", cnpj);
		
		Loja entity;
		try {
			entity = findByIdQuery.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}	
		
		return entity;
	}

	@GET
	@Produces("application/json")
	public List<LojaDto> listAll(@QueryParam("start") Integer startPosition, @QueryParam("max") Integer maxResult) {
		
		TypedQuery<Loja> findAllQuery = em.createQuery(
				"SELECT DISTINCT l FROM Loja l LEFT JOIN FETCH l.segmentos s ORDER BY l.id", Loja.class);
		
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		
		final List<Loja> results = findAllQuery.getResultList();
		List<LojaDto> dtos = new ArrayList<LojaDto>();		
		results.forEach(res -> dtos.add(this.convertToDto(res)));		
		
		return dtos;
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes("application/json")
	public Response update(@PathParam("id") Long id, LojaDto lojaDto) {
		
		if (lojaDto == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (id == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Loja lojaEnt = em.find(Loja.class, id);  
		if (lojaEnt == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		try {
			this.convertFromDto(lojaDto, lojaEnt);
			List<Segmento> segmentos = new ArrayList<Segmento>(); 
			if(lojaDto.getSegmentos() == null) {
				segmentos = em.createNamedQuery("Segmento.findByIdsNotIn").setParameter("ids", new HashSet<Segmento>()).getResultList();				
			}
			else if(!lojaDto.getSegmentos().isEmpty()) {
				segmentos = em.createNamedQuery("Segmento.findByIdsNotIn").setParameter("ids", lojaDto.getSegmentos()).getResultList();
			}
			segmentos.forEach(seg -> seg.getLojas().remove(lojaEnt));
			lojaEnt.getSegmentos().forEach(seg -> seg.getLojas().add(lojaEnt));
			em.merge(lojaEnt);
		} catch (OptimisticLockException e) {
			return Response.status(Response.Status.CONFLICT)
					.entity(e.getEntity()).build();
		}

		return Response.noContent().build();
	}
	
	@GET
	@Path("/audit/{id}")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public List<LojaDto> audit(@PathParam("id") Long id) {
		
		AuditReader reader = AuditReaderFactory.get(em);
		List<Loja> lojasEnt = reader.createQuery().forRevisionsOfEntity(Loja.class, true, true)
				.add(AuditEntity.id().eq(id))
				.add(AuditEntity.revisionType().eq(RevisionType.DEL))
				.getResultList();
		List<LojaDto> lojasDto = new ArrayList<LojaDto>();
		lojasEnt.forEach(lojE -> lojasDto.add(convertToDto(lojE)));
		return lojasDto;
	}
	
	
	public List<String> validate(LojaDto loja){
		
		List<String> erros = new ArrayList<String>();		
		
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();		
		validator.validate(loja).forEach((violation) -> {
			erros.add(violation.getPropertyPath() + ": " + violation.getMessage());			  
		});
		
		List<Segmento> segmentos = em.createNamedQuery("Segmento.findByIds").setParameter("ids", loja.getSegmentos()).getResultList();
		if(segmentos.size() < loja.getSegmentos().size())
			erros.add("segmentos: segmento(s) informado(s) não cadastradado(s)");
		
		
		if(this.findBCnpj(loja.getCnpj()) != null)
			erros.add("cnpj: CNPJ já cadastrado");
			
		return erros;		
	}
	
	@SuppressWarnings("unchecked")
	public Loja convertFromDto(LojaDto lojaDto, Loja LojaEnt) {
		if(lojaDto.getId() != null)
			LojaEnt.setId(lojaDto.getId());
		if(lojaDto.getCnpj() != null && !lojaDto.getCnpj().isEmpty() && lojaDto.getCnpj() != null)
			LojaEnt.setCnpj(lojaDto.getCnpj());
		if(lojaDto.getPiso() != null && !lojaDto.getPiso().isEmpty() && lojaDto.getPiso() != null)
			LojaEnt.setPiso(lojaDto.getPiso());
		if(lojaDto.getNumero() != null && !lojaDto.getNumero().isEmpty() && lojaDto.getNumero() != null)
			LojaEnt.setNumero(lojaDto.getNumero());
		if(lojaDto.getSegmentos() == null)
			LojaEnt.setSegmentos(em.createNamedQuery("Segmento.findByIds").setParameter("ids", new HashSet<Segmento>()).getResultList());
		else 
			if(!lojaDto.getSegmentos().isEmpty())
				LojaEnt.setSegmentos(em.createNamedQuery("Segmento.findByIds").setParameter("ids", lojaDto.getSegmentos()).getResultList());
		LojaEnt.setCreated(LojaEnt.getCreated());
		LojaEnt.setUpdated(LojaEnt.getUpdated());
		return LojaEnt;
	}
	
	private LojaDto convertToDto(Loja lojaEnt) {
		LojaDto lojaDto = new LojaDto();
		lojaDto.setId(lojaEnt.getId());
		lojaDto.setCnpj(lojaEnt.getCnpj());
		lojaDto.setPiso(lojaEnt.getPiso());
		lojaDto.setNumero(lojaEnt.getNumero());
		lojaEnt.getSegmentos().forEach(seg -> lojaDto.getSegmentos().add(seg.getId()));
		DateFormat f = new SimpleDateFormat("dd-MM-yyyy' 'HH:mm:ss");
		lojaDto.setCreated(f.format(lojaEnt.getCreated()));
		lojaDto.setUpdated(f.format(lojaEnt.getUpdated()));		
		return lojaDto;
	}
}
