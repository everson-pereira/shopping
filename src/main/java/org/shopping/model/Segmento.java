package org.shopping.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

@Entity
@XmlRootElement
@NamedQueries({
	@NamedQuery(name="Segmento.findByIds", query="SELECT s FROM Segmento s WHERE s.id IN :ids "),
	@NamedQuery(name="Segmento.findByIdsNotIn", query="SELECT s FROM Segmento s WHERE s.id NOT IN :ids")
})
@Audited
public class Segmento implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Column
	private String descricao;
	@Version
	@Column(name = "version")
	private int version;

	@ManyToMany	
	private Set<Loja> lojas = new HashSet<Loja>();

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Segmento)) {
			return false;
		}
		Segmento other = (Segmento) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (descricao != null && !descricao.trim().isEmpty())
			result += "descricao: " + descricao;
		return result;
	}

	public Set<Loja> getLojas() {
		return this.lojas;
	}

	public void setLojas(final Set<Loja> lojas) {
		this.lojas = lojas;
	}

}