package org.shopping.model.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.br.CNPJ;

@XmlRootElement
public class LojaDto implements Serializable {

	private static final long serialVersionUID = 1L;
			
	private Long id;
	
	@NotNull(message="Campo Obrigatório")
	@CNPJ(message="CNPJ inválido")
	private String cnpj;
	
	@NotNull(message="Campo Obrigatório")
	private String piso;
	
	@NotNull(message="Campo Obrigatório")
	private String numero;
	
	private Set<Long> segmentos = new HashSet<Long>();
	
	private String created;
	
	private String updated;
	
	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cpf) {
		this.cnpj = cpf;
	}
			
	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Set<Long> getSegmentos() {
		return this.segmentos;
	}

	public void setSegmentos(final Set<Long> segmentos) {
		this.segmentos = segmentos;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof LojaDto)) {
			return false;
		}
		LojaDto other = (LojaDto) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (cnpj != null && !cnpj.trim().isEmpty())
			result += "cpf: " + cnpj;
		return result;
	}
}